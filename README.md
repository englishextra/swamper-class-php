swamper-class-php
=================

General purpose PHP class to work with strings and files

What it does
------------

The PHP class that contains methods to work with strings and files. Function names determine clearly what the method is doing:

1. is_utf8
2. safe_str
3. get_post
4. ensure_amp
5. ensure_lt_gt
6. ord_space
7. ord_underscore
8. ord_hypher
9. ord_newline
10. remove_tags
11. remove_ents
12. remove_comments
13. has_http
14. is_ip
15. write_file
16. clear_data
17. remove_dir_content
18. remove_bbcoded
19. symbs_to_ents
20. safe_html
21. random_anchor

Dependencies
------------

PHP 5.2

Usage
-----

    $relpa = ($relpa0 = preg_replace("/[\/]+/", "/", $SERVER['DOCUMENT_ROOT'] . '/')) ? $relpa0 : '';

    require_once $relpa . 'lib/swamper.class.php';

    if (!isset($Swamper) || empty($Swamper)) {

	    $Swamper = new Swamper ();

    }

    $ttl = isset($GET['ttl']) ? $GET['ttl'] : '';

    $ttl = $Swamper->ensure_amp($ttl);
	
Authors
-------

**Serguei Shimansky <serguei@shimansky.biz>**

+ https://gist.github.com/2981888

+ http://pastebin.com/y2Gs4bzE

+ https://twitter.com/englishextra

+ https://bitbucket.org/englishextra

Copyright and License
---------------------

Copyright 2012 Serguei Shimansky
